/*
 * Printing args with forEach
 * Run using: 
 * scala _06_ExplicitForEach.scala Explicit is great
 */
args.foreach((arg: String) => println(arg))