/*
 * Printing args with forEach
 * Run using: 
 * scala _05_ForEach.scala Scala is awesome
 */
args.foreach(arg => println(arg))