/*
 * While loop printing all parameters.
 * Run using: 
 * scala _03_WhileLoop.scala Scala is awesome
 */
var i = 0
while(i < args.length){
  println(args(i))
  i += 1
}