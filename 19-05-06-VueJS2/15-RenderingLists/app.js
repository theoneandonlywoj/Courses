new Vue({
  el: '#app',
  data: {
    ingredients: ['meat', 'fruit', 'cookies'],
    people: [
      {
        name: 'Wojciech',
        age: 27,
        color: 'blue'
      },
      {
        name: 'Jessie',
        age: 'unknown',
        color: 'yellow'
      }
    ]
  }
})
