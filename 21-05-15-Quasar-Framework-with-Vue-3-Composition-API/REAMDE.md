# Quasar Framework with Vue 3 Composition API
https://www.youtube.com/watch?v=qPkSwo8QyoA&ab_channel=CodingwithJustin

## Installing Quasar CLI
```bash 
sudo npm install -g @quasar/cli
```

## Creating a new project with Vue 3
```bash
quasar create note-app --branch next
```

## Start dev server
```bash
quasar dev
```