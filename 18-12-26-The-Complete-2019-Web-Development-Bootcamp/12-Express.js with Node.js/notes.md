# Intializing NPM
```terminal
npm init
```
# Installing Express.js
```terminal
npm install express
```
# Installing Nodemon
```terminal
npm install -g nodemon
```
# Using Nodemon
```terminal
nodemon server.js localhost 3000
```
