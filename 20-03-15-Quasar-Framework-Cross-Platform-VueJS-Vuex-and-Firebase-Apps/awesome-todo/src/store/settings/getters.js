export function someSettingsGetter (state) {
  return state.settings
}

export function settingsSomeLocalStorageSettingGetter (state) {
  return state.settings.someLocalStorageSetting
}
