export default function () {
  return {
    search: '',
    sortBy: 'name',
    tasks: {},
    tasksDownloaded: false
  }
}
