" .vimrc by Wojciech Orzechowski
" Contact: theoneandonlywoj@gmail.com
" MacOS specific - yanked text will be available in the clipboard.
set clipboard=unnamed
" Turn on line numbering.
set number
" Auto reload the file if it was changed by an external command.
set autoread
