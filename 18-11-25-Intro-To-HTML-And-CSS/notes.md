# Intro to HTML and CSS
## Resources
https://developer.mozilla.org/en-US/docs/Web/HTML/Element

## CSS

### Tag Selector
```css
h1 {
  color: green;
}
```
### class Attribute Selector
```css
.book-summary {
  color: blue;
}
```
### id attribute selector
```css
#site-description {
  color: red;
}
```
### Specifying a color
#### RGB
```css
body {
	background-color: rgb(255, 0, 255);
}
```
#### Hexadecimal
```css
body {
	background-color: #ff00ff;
}
```